#include <iostream>
#include <algorithm>
#include "ConsoleColor.h"

using namespace std;

string teclado(const string &legenda) {
    string entrada;
    while (true) {
        cout << "\n:" << legenda << endl;
        cout << "Escolha " << yellow << "Pedra ,Papel ou Tesoura " << white << ":";
        getline(cin, entrada);
        transform(entrada.begin(), entrada.end(), entrada.begin(), ::tolower);
        if (entrada == "pedra" || entrada == "papel" || entrada == "tesoura") {
            cout << legenda << " escolheu " << entrada << endl;
            return entrada;
        } else {
            cout << red << "Entrada invalida" << white << endl;
        }
    }
}

void play(const string &p1, const string &p2) {
    string retorno;
    if ((p1 == "pedra" && p2 == "tesoura") || (p1 == "tesoura" && p2 == "papel") || (p1 == "papel" && p2 == "pedra")) {
        retorno = "Jogador 1";
    }else {
        retorno = "jogador 2";
    }

    cout << blue << "\n:" <<  retorno << " vence \n" << white  << endl;
}

int main() {

    //system("clear");
    string jogador1 = teclado("Jogador 1");
    string jogador2 = teclado("Jogador 2");


    cout << "\n\n:Jogador 1 escolheu : " << jogador1 << endl;
    cout << ":Jogador 2 escolheu : " << jogador2 << endl;


    if (jogador1 == jogador2) {
        cout << ":Empate \n";
    } else {
        play(jogador1, jogador2);
    }
    return 0;
}